import React from 'react';
import './App.css';
import NavBar from "./shared-module/components/Navbar";
import Main from "./shared-module/components/Main";
import Footer from "./shared-module/components/Footer";
import {BrowserRouter, Routes, Route, Navigate} from "react-router-dom";
import {AuthProvider} from "./auth-module/providers/AuthProvider";
import AppPage from './app-module/components/AppPage';
import ChatPage from "./chat-module/components/ChatPage";
import {Provider} from "react-redux";
import {store} from "./shared-module/store/store";
import {RequireAuth} from "./auth-module/components/RequireAuth";
import TapeForm from "./tapes-module/components/TapeForm";
import Recs from "./tapes-module/components/Recs";
import ReceivedLikesPage from "./likes-module/components/ReceivedLikesPage";
import SentLikesPage from "./likes-module/components/SentLikesPage";
import Profile from "./app-module/components/Profile";
import CreateTapeForm from "./tapes-module/components/CreateTapeForm";

function App() {

   return (
       <Provider store={store}>
          <AuthProvider>
             <BrowserRouter>
                <NavBar/>
                <Routes>
                   <Route index element={<Main/>}/>
                   <Route path="app" element={<RequireAuth><AppPage/></RequireAuth>}>
                      <Route path="" element={<Navigate to="recs"/>}/>
                      <Route path="recs" element={<Recs/>}/>
                      <Route path="received-likes" element={<ReceivedLikesPage/>}/>
                      <Route path="sent-likes" element={<SentLikesPage/>}/>
                      <Route path="create-tape" element={<TapeForm/>}/>
                      <Route path="messages/:chatId" element={<ChatPage/>}/>
                      <Route path="profiles/:userId" element={<Profile/>}>
                         <Route path="create-tape" element={<CreateTapeForm/>}/>
                      </Route>
                      <Route path="*" element={<div>404</div>}/>
                   </Route>
                   <Route path="profile" element={<Main/>}/>
                   <Route path="*" element={<div>404</div>}/>
                </Routes>
                <Footer/>
             </BrowserRouter>
          </AuthProvider>
       </Provider>
   );
}

export default App;
