import {Socket} from "socket.io-client";
import {useEffect} from "react";
import {IMessage} from "../../shared-module/models/message";
import {useAppDispatch, useAppSelector} from "../../shared-module/store/hooks";
import {selectChats} from "../reducers/chat-reducer";
import {addMessage, deleteMessage, editMessage} from "../reducers/message-reducer";
import {FromServerEvents} from "../constants/socket-events";


export const useRegisterSocketEvents = (socket: Socket | null) => {

    const {currentChat} = useAppSelector(selectChats)
    const dispatch = useAppDispatch();

    const onMessage = (message: IMessage) => {
        console.log(`NEW MESSAGE in chat ${message.chatId}`)
        const chatId = currentChat?.id;
        console.log(chatId === message.chatId)
        if (chatId === message.chatId) dispatch(addMessage(message))
        // dispatch(setLastMessage({message: msg, chatId: msg.chatId}))
        // dispatch(incrementUnread({chatId: msg.chatId}))
    }

    const onDelete = (message: IMessage) => {
        console.log(`DELETE MESSAGE in chat ${message.chatId}`)
        const chatId = currentChat?.id;
        if (chatId === message.chatId) dispatch(deleteMessage(message.id))
    };

    const onEdit = (message: IMessage) => {
        console.log(`EDIT MESSAGE in chat ${message.chatId}`)
        const chatId = currentChat?.id;
        if (chatId === message.chatId) dispatch(editMessage(message))
    };

    useEffect(() => {
        if (!socket) return;
        socket.on(FromServerEvents.NEW_MESSAGE, onMessage);
        socket.on(FromServerEvents.EDITED_MESSAGE, onEdit);
        socket.on(FromServerEvents.DELETED_MESSAGE, onDelete);
        return () => {
            socket.off(FromServerEvents.NEW_MESSAGE, onMessage);
            socket.off(FromServerEvents.EDITED_MESSAGE, onEdit);
            socket.off(FromServerEvents.DELETED_MESSAGE, onDelete);
        };
    }, [socket]);
}
