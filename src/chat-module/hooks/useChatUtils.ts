import {useAuth} from "../../auth-module/providers/AuthProvider";
import {IMessage} from "../../shared-module/models/message";


export const useChatUtils = () => {
   const {user} = useAuth();

   const isSelf = (message: IMessage) => message.authorId === user?.id;

   return {isSelf}
}
