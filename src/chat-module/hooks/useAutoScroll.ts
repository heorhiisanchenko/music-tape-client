import {useAppSelector} from "../../shared-module/store/hooks";
import {selectMessages} from "../reducers/message-reducer";
import {useEffect, useRef} from "react";
import {useChatUtils} from "./useChatUtils";

export const useAutoScroll = () => {
   const {messages} = useAppSelector(selectMessages);

   const scrollRef = useRef<HTMLUListElement>(null);

   const {isSelf} = useChatUtils()

   // jump to bottom when new message is added
   useEffect(() => {
      const list = scrollRef.current;
      if (list && messages?.length && isSelf(messages.slice(-1)[0])) {
         list.scrollTop = list.scrollHeight;
      }
   }, [messages.length])

   return scrollRef;
}
