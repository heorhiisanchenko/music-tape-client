import {IChat} from "../../shared-module/models/chat";
import {IMessage, MessageStatus, NoId} from "../../shared-module/models/message";
import {useAppDispatch} from "../../shared-module/store/hooks";
import dayjs from "dayjs";
import {useChatContext} from "../providers/SocketProvider";
import {FromClientEvents} from "../constants/socket-events";
import chatApi from "../../shared-module/api/chat-api";
import {addMessage, deleteMessage, editMessage} from "../reducers/message-reducer";

export const useEmitSocketEvents = (currentChat: IChat) => {
   const dispatch = useAppDispatch();
   const {socket, user} = useChatContext();

   const createMessage = (text: string): NoId<IMessage> => {
      const authorId = user.id;
      const {user1, user2} = currentChat;
      const recipientId = user1 === authorId ? user2 : user1
      return {
         authorId,
         recipientId,
         timestamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
         chatId: currentChat.id,
         content: text,
         status: MessageStatus.SENDING,
         edited: false
      }
   }

   const handleSend = async (text: string) => {
      const noIdMessage = createMessage(text)
      const fullMessage = await chatApi.postMessage(noIdMessage);
      console.log(fullMessage)
      console.log(socket)
      if (socket) {
         socket.emit(FromClientEvents.NEW_MESSAGE, fullMessage)
         console.log("EMITTED message " + fullMessage.content)
      }
      if (currentChat.id === fullMessage.chatId) dispatch(addMessage(fullMessage))
   }

   const handleEdit = (message: IMessage, editedText: string) => {
      const editedMessage = {...message, content: editedText, edited: true}
      if (socket) {
         socket.emit(FromClientEvents.EDITED_MESSAGE, message)
         console.log("EMITTED message " + editedMessage.content)
      }
      if (currentChat.id === editedMessage.chatId) dispatch(editMessage(editedMessage))
   }

   const handleDelete = (message: IMessage) => {
      if (socket) {
         socket.emit(FromClientEvents.DELETED_MESSAGE, message)
         console.log("DELETED message " + message.content)
      }
      if (currentChat.id === message.chatId) dispatch(deleteMessage(message.id))
   }

   return {handleSend, handleEdit, handleDelete};
}
