import {Dispatch, SetStateAction, useEffect, useState} from "react";

export const useContextMenu = () => {
   const [menuId, setMenuId] = useState<number | null>(null);

   useEffect(() => {
      const callback = () => setMenuId(null)
      window.addEventListener('contextmenu', callback)
      window.addEventListener('click', callback)
      return () => {
         window.removeEventListener('contextmenu', callback)
         window.removeEventListener('click', callback)
      }
   }, [])

   return [menuId, setMenuId] as
       [number | null, Dispatch<SetStateAction<number | null>>];
}
