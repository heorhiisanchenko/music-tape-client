import {io, Socket} from 'socket.io-client'
import {createContext, FC, PropsWithChildren, useContext, useEffect, useState} from "react";
import {FromClientEvents} from "../constants/socket-events";
import {urls} from "../../shared-module/constants/api";
import {useRegisterSocketEvents} from "../hooks/useRegisterSocketEvents";
import {IUser} from "../../shared-module/models/user";

interface IChatContext {
   socket: Socket | null,
   user: IUser
}

const ChatContext = createContext<IChatContext>(null!);

interface IProps {
   user: IUser
}

export const SocketProvider: FC<PropsWithChildren<IProps>> = ({user, children}) => {
   const [socket, setSocket] = useState<Socket | null>(null)

   useEffect(() => {
      const newSocket = io(`${urls.CHATS}`);
      newSocket.emit(FromClientEvents.USER_CONNECT, user.id);
      setSocket(newSocket)
      return () => {
         newSocket.close()
      };
   }, [setSocket])

   useRegisterSocketEvents(socket);

   return <ChatContext.Provider value={{socket, user}}>{children}</ChatContext.Provider>
}

export const useChatContext = () => useContext(ChatContext)


