import {createContext, Dispatch, FC, PropsWithChildren, SetStateAction, useContext, useState} from "react";
import {IChat} from "../../shared-module/models/chat";
import {useContextMenu} from "../hooks/useContextMenu";

interface ICurrentChatContext {
   currentChat: IChat
   menuId: number | null
   setMenuId: Dispatch<SetStateAction<number | null>>
}

const CurrentChatContext = createContext<ICurrentChatContext>(null!);

interface IProps {
   currentChat: IChat
}

export const CurrentChatProvider: FC<PropsWithChildren<IProps>> =
    ({
        currentChat,
        children
     }) => {
       const [menuId, setMenuId] = useContextMenu()

       return <CurrentChatContext.Provider value={{currentChat, menuId, setMenuId}}>
          {children}
       </CurrentChatContext.Provider>
    }

export const useCurrentChatContext = () => useContext(CurrentChatContext)


