import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IMessage} from "../../shared-module/models/message";
import {RootState} from "../../shared-module/store/store";

interface MessageState {
    messages: IMessage[]
    loading: boolean
}

const initialState: MessageState = {
    messages: [],
    loading: false,
}

const messageSlice = createSlice({
    name: 'messages',
    initialState,
    reducers: {
        setLoading: (state, {payload}: PayloadAction<boolean>) => {
            state.loading = payload;
        },
        setMessages: (state, {payload}: PayloadAction<IMessage[]>) => {
            state.messages = payload;
        },
        addMessage: (state, {payload}: PayloadAction<IMessage>) => {
            state.messages.push(payload);
        },
        editMessage: (state, {payload}: PayloadAction<IMessage>) => {
            const index = state.messages.findIndex(message => message.id === payload.id);
            state.messages[index] = payload
        },
        deleteMessage: (state, {payload}: PayloadAction<number>) => {
            state.messages = state.messages.filter(message => message.id !== payload);
        },
    }
})

export const {
    setLoading,
    setMessages,
    addMessage,
    editMessage,
    deleteMessage
} = messageSlice.actions;

export const selectMessages = (state: RootState) => state.messages
export default messageSlice.reducer
