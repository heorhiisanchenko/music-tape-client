import {createAsyncThunk} from "@reduxjs/toolkit";
import {setLoading, setMessages} from "./message-reducer";
import ChatApi from "../../shared-module/api/chat-api";

export const messageThunk = createAsyncThunk('/messages/get',
    async (id: number, thunkApi) => {
        try {
            thunkApi.dispatch(setLoading(true))
            const messages = await ChatApi.getMessages(id);
            thunkApi.dispatch(setMessages(messages))
        } catch (e) {
            console.log(e)
        } finally {
            thunkApi.dispatch(setLoading(false));
        }
    })
