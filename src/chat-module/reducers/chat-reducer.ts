import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IChat} from "../../shared-module/models/chat";
import {RootState} from "../../shared-module/store/store";

interface ChatState {
    chats: IChat[];
    loading: boolean;
    currentChat: IChat | null
}

const initialState: ChatState = {
    chats: [],
    loading: false,
    currentChat: null
}

const chatSlice = createSlice({
    name: 'chats',
    initialState,
    reducers: {
        setLoading: (state, {payload}: PayloadAction<boolean>) => {
            state.loading = payload;
        },
        setChats: (state, {payload}: PayloadAction<IChat[]>) => {
            state.chats = payload;
        },
        setCurrentChat: (state, {payload}: PayloadAction<IChat>) => {
            state.currentChat = payload;
        },
    }
})

export const {
    setLoading,
    setChats,
    setCurrentChat,
} = chatSlice.actions;

export const selectChats = (state: RootState) => state.chats

export default chatSlice.reducer
