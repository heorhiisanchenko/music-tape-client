import {createAsyncThunk} from "@reduxjs/toolkit";
import {setChats, setLoading} from "./chat-reducer";
import ChatApi from "../../shared-module/api/chat-api";

export const chatThunk = createAsyncThunk('/chats/get',
    async (_, thunkApi) => {
        try {
            thunkApi.dispatch(setLoading(true))
            const chats = await ChatApi.getChats();
            thunkApi.dispatch(setChats(chats))
        } catch (e) {
            console.log(e)
        } finally {
            thunkApi.dispatch(setLoading(false));
        }
    })
