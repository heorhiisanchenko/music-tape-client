import {FC, MouseEvent} from 'react';
import {useAppDispatch} from "../../shared-module/store/hooks";
import {deleteMessage, editMessage} from "../reducers/message-reducer";
import {IMessage} from "../../shared-module/models/message";
import {useEmitSocketEvents} from "../hooks/useEmitSocketEvents";
import {useCurrentChatContext} from "../providers/CurrentChatProvider";
import './styles/ContextMenu.css'
import {stopEvent} from "../../shared-module/utils/event";

interface IProps {
   message: IMessage
}

const ContextMenu: FC<IProps> = ({message}) => {

   const dispatch = useAppDispatch()
   const {currentChat} = useCurrentChatContext()
   const {handleEdit, handleDelete} = useEmitSocketEvents(currentChat)

   const onEdit = async () => {
      dispatch(editMessage(message))
      handleDelete(message)
   }

   const onDelete = async () => {
      dispatch(deleteMessage(message.id))
      handleDelete(message)
   }

   return (
       <ul className="context_menu_container p-0">
          <li className="context_menu_item" onClick={stopEvent(onDelete)}>Delete</li>
       </ul>
   );
};

export default ContextMenu;
