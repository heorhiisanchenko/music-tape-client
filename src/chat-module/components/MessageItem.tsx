import {FC, useEffect, useState} from 'react';
import {IMessage} from "../../shared-module/models/message";
import {formatDateTime} from "../../shared-module/utils/format";
import ContextMenu from "./ContextMenu";
import {useContextMenu} from "../hooks/useContextMenu";
import {stopEvent} from "../../shared-module/utils/event";
import {useCurrentChatContext} from "../providers/CurrentChatProvider";

interface IProps {
   message: IMessage,
   isSelf: boolean
}

const MessageItem: FC<IProps> = ({message, isSelf}) => {

   const {menuId, setMenuId} = useCurrentChatContext()
   const showMenu = menuId === message.id

   return (
       <li
           className={"message_list_item " + (isSelf ? 'self' : '')}
           onContextMenu={stopEvent(() => setMenuId(message.id))}
       >
          <ul className="message_item">
             <li className="message_text">
                {message.content}
             </li>
             <li className="message_info">
                <span className="message_timestamp">
                    {formatDateTime(message.timestamp, 'HH:mm')}
                </span>
             </li>
             <div className="clearfix">
             </div>
          </ul>
          {isSelf && showMenu && <ContextMenu message={message}/>}
       </li>
   );
};

export default MessageItem;
