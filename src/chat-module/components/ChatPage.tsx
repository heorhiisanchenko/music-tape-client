import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../../shared-module/store/hooks";
import {selectMessages} from "../reducers/message-reducer";
import {messageThunk} from '../reducers/message-thunk';
import './styles/ChatPage.css'
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import {selectChats} from "../reducers/chat-reducer";
import {CurrentChatProvider} from "../providers/CurrentChatProvider";
import blank_profile from '../../assets/blank-profile.png'
import {useAuth} from "../../auth-module/providers/AuthProvider";
import {getOtherUser} from "../../shared-module/utils/chats";
import MessageTopBar from "./MessageTopBar";

const ChatPage = () => {

   const {chatId} = useParams();
   const {messages, loading} = useAppSelector(selectMessages);
   const {currentChat} = useAppSelector(selectChats);
   const {user} = useAuth();
   const dispatch = useAppDispatch();

   useEffect(() => {
      const numId = parseInt(chatId || "");
      if (numId) dispatch(messageThunk(numId))
   }, [chatId]);

   if (!currentChat || !user) return null;

   return (
       <CurrentChatProvider currentChat={currentChat}>
          <main className="current_chat_container">
             <div className="current_chat flex-grow-1 border d-flex flex-column">
                {/*<MessageTopBar/>*/}
                <MessageList messages={messages} loading={loading}/>
                <MessageInput currentChat={currentChat}/>
             </div>
             <div className="chat_right_sidebar border">
                <img className="chat_sidebar_profile_image"
                     src={blank_profile}
                     alt={`User ${getOtherUser(currentChat, user.id)}'s profile picture`}/>
             </div>
          </main>
       </CurrentChatProvider>
   );
};

export default ChatPage;
