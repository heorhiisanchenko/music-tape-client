import {FC} from 'react';
import Loader from "../../shared-module/components/Loader";
import {IMessage} from "../../shared-module/models/message";
import {useChatUtils} from "../hooks/useChatUtils";
import {useAutoScroll} from "../hooks/useAutoScroll";
import MessageItem from "./MessageItem";
import './styles/MessageList.css'

interface IProps {
   messages: IMessage[]
   loading: boolean
}

const MessageList: FC<IProps> = ({loading, messages}) => {

   const {isSelf} = useChatUtils();
   const scrollRef = useAutoScroll();

   return (
       <div className="message_list_wrapper">
          {loading || !messages ? <Loader/> :
              <ul
                  className={"message_list" + (loading ? ' msg-loading' : '')}
                  ref={scrollRef}
                  // onScroll={handleScroll}
              >
                 {messages.map(message =>
                     <MessageItem key={message.id} message={message} isSelf={isSelf(message)}/>)
                 }
              </ul>
          }
       </div>
   );
};

export default MessageList;
