import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import './styles/ChatList.css'
import {useAppDispatch, useAppSelector} from "../../shared-module/store/hooks";
import {selectChats, setCurrentChat} from "../reducers/chat-reducer";
import {chatThunk} from "../reducers/chat-thunk";
import {useChatContext} from "../providers/SocketProvider";
import {IChat} from "../../shared-module/models/chat";
import {getOtherUser} from "../../shared-module/utils/chats";

const ChatList = () => {

   const {chats} = useAppSelector(selectChats);
   const dispatch = useAppDispatch();
   const {user} = useChatContext();

   useEffect(() => {
      dispatch(chatThunk());
   }, [])

   if (!user || !Array.isArray(chats)) return null;

   return (
       <ul className="m-0 p-0">
          {chats.map(chat =>
              <li className="chat_item d-flex" key={chat.id}>
                 <NavLink to={'messages/' + chat.id} onClick={() => dispatch(setCurrentChat(chat))}
                          className="chat_link d-flex flex-row flex-grow-1 "
                 >
                    <div className="chat_avatar">
                       <span className="chat_avatar_text">
                          {getOtherUser(chat, user.id)}
                       </span>
                    </div>
                    <div className="flex-grow-1 chat_user_container">
                       <div className="chat_user">
                          User {getOtherUser(chat, user.id)}
                       </div>
                    </div>
                 </NavLink>
              </li>
          )}
       </ul>
   );
};

export default ChatList;
