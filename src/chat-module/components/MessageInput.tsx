import {useState, FC, MouseEvent} from 'react';
import {useEmitSocketEvents} from "../hooks/useEmitSocketEvents";
import {IChat} from "../../shared-module/models/chat";
import SendButton from "../../shared-module/components/SendButton";
import {MicroIcon} from "../../shared-module/components/Icons";
import './styles/MessageInput.css'

interface IProps {
   currentChat: IChat
}

const MessageInput: FC<IProps> = ({currentChat}) => {

   const [text, setText] = useState<string>('');

   const {handleSend} = useEmitSocketEvents(currentChat)

   const handleSubmit = async (e: MouseEvent<HTMLElement>) => {
      e.preventDefault()
      setText('')
      await handleSend(text)
   }

   return (
       <div className="main_chat_input_container">
          <form className="main_chat_input_form">
             <input className="main_chat_input"
                    type="text"
                    placeholder=" Write a message..."
                    value={text}
                    onChange={e => setText(e.target.value)}
             />
             {text ? <SendButton handleSubmit={handleSubmit}/> : <MicroIcon/>}
          </form>
       </div>
   );
};

export default MessageInput;

