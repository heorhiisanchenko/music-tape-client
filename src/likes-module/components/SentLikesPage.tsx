import React, {useEffect, useState} from 'react';
import {ILike, LikeStatus} from "../../shared-module/models/likes";
import Loader from "../../shared-module/components/Loader";
import likesApi from "../../shared-module/api/likes-api";
import {NavLink} from "react-router-dom";
import blank_profile from "../../assets/blank-profile.png";

const SentLikesPage = () => {

   const [likes, setLikes] = useState<ILike[]>();

   useEffect(() => {
      likesApi.getSentLikes().then(matches => {
             if (Array.isArray(matches))
                setLikes(matches)
          }
      )
   }, []);

   return (
       <main className="likes_page_container">
          <div className="likes_scroll_container">
             <div className="likes_list">
                {!likes ? <Loader/> : likes.map((like) => (
                    <div className="like_card" key={like.id}>
                       <img className="like_user_pic"
                            src={blank_profile}
                            alt={`User ${like.userId}'s picture`}
                       />
                       <NavLink className="like_user_link" to={"/app/profiles/" + like.likedUserId}>
                          User {like.likedUserId}
                       </NavLink>
                    </div>
                ))}
             </div>
          </div>
       </main>
   );
};

export default SentLikesPage;
