import React, {useEffect, useState} from 'react';
import {ILike, LikeStatus} from "../../shared-module/models/likes";
import Loader from "../../shared-module/components/Loader";
import LikeControls from "../../shared-module/components/LikeControls";
import likesApi from "../../shared-module/api/likes-api";
import chatApi from "../../shared-module/api/chat-api";
import {useAuth} from "../../auth-module/providers/AuthProvider";
import './styles/Likes.css'
import {NavLink} from "react-router-dom";
import blank_profile from '../../assets/blank-profile.png'

const ReceivedLikesPage = () => {

   const [likes, setLikes] = useState<ILike[]>();
   const {user} = useAuth();

   useEffect(() => {
      likesApi.getReceivedLikes().then(likes => {
             if (Array.isArray(likes))
                setLikes(likes)
          }
      )
   }, []);

   const handleLike = async (like: ILike) => {
      await likesApi.editLike({
         id: like.id,
         status: LikeStatus.APPROVED
      })
      if (!user) return;
      const chat = await chatApi.createChat({
         user1: user.id,
         user2: like.userId
      })
      console.log(chat)
   }

   const handleReject = async (like: ILike) => {
      await likesApi.editLike({
         id: like.id,
         status: LikeStatus.REJECTED
      })
   }

   return (
       <main className="likes_page_container">
          <div className="likes_scroll_container">
             <div className="likes_list">
                {
                   !likes ? <Loader/> : likes.map((like) => (
                       <div className="like_card" key={like.id}>
                          <img className="like_user_pic" src={blank_profile} alt={`User ${like.userId}'s picture`}/>
                          <NavLink className="like_user_link" to={"/app/profiles/" + like.userId}>
                             User {like.userId}
                          </NavLink>
                          <LikeControls
                              className="received_like_controls"
                              handleLike={() => handleLike(like)}
                              handleReject={() => handleReject(like)}
                          />
                       </div>
                   ))
                }
             </div>
          </div>
       </main>
   );
};

export default ReceivedLikesPage;
