import React from 'react';
import MatchSidebar from "./MatchSidebar";
import {Outlet} from 'react-router-dom'
import {SocketProvider} from "../../chat-module/providers/SocketProvider";
import {useAuth} from "../../auth-module/providers/AuthProvider";

const AppPage = () => {

   const {user} = useAuth();

   if (!user) return null;

   return (
       <SocketProvider user={user}>
          <div className="container-fluid flex-grow-1 p-0 d-flex">
             <div className="border sidebar_left">
                <MatchSidebar/>
             </div>
             <Outlet/>
          </div>
       </SocketProvider>
   );
};

export default AppPage;
