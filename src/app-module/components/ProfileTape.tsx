import React, {useEffect, useState} from 'react';
import {ITape} from "../../shared-module/models/tapes";
import {useLoadFile} from "../../tapes-module/hooks/useLoadFile";
import {useAuth} from "../../auth-module/providers/AuthProvider";
import Loader from "../../shared-module/components/Loader";

const ProfileTape = ({tape}: { tape: ITape }) => {

   const {file, fetchFile} = useLoadFile();
   const {user} = useAuth();

   useEffect(() => {
      fetchFile(tape.path)
   }, [])


   return (
       <div className="profile_tape_container">
          {file ?
              <video className="profile_tape_video_container" muted controls>
                 <source src={tape.path} type="video"/>
                 Video could not be played :(((
              </video>
              : <Loader/>
          }
          <div>
             {tape.description}
          </div>
       </div>
   );
};

export default ProfileTape;
