import React, {useEffect, useState} from 'react';
import {NavLink, useParams, Outlet} from "react-router-dom";
import Loader from "../../shared-module/components/Loader";
import {ITape} from "../../shared-module/models/tapes";
import tapeApi from "../../shared-module/api/tape-api";
import ClosePageButton from "../../shared-module/components/ClosePageButton";
import './styles/Profile.css'
import blank_profile from '../../assets/blank-profile.png'
import {useAuth} from "../../auth-module/providers/AuthProvider";
import ProfileTape from "./ProfileTape";

const Profile = () => {

   const {userId} = useParams();
   const numId = parseInt(userId || "");

   const [tapes, setTapes] = useState<ITape[]>();
   const [loading, setLoading] = useState<boolean>(false);

   const {user} = useAuth();

   useEffect(() => {
      setLoading(true);
      if (numId) tapeApi.getTapesForUser(numId)
          .then(tapes => {
             if (Array.isArray(tapes)) setTapes(tapes)
             setLoading(false);
          })
   }, [])

   if (!user) return null;


   const noTapesMessage = numId !== user.id ?
       <div>This user has no tapes yet</div> :
       <div>
          <div>You have no tapes yet</div>
          <NavLink to={'create-tape'}>Create one</NavLink>
       </div>

   return (
       <div className="profile_page_container">
          <div className="profile_top_bar">
             <img className="profile_avatar_image"
                  src={blank_profile}
                  alt={`User ${userId}'s profile picture`}/>
             <div className="profile_username">
                User {userId}
             </div>
             <div className="profile_username">
                {numId === user.id &&
                    <NavLink to={'create-tape'}>Create tape</NavLink>}
             </div>
          </div>
          <div className="profile_tapes_container">
             {loading || !tapes ? <Loader/> :
                 !tapes.length ? noTapesMessage :
                     <div className="profile_tapes_list">{
                        tapes.map((tape) => (
                            <ProfileTape tape={tape}/>
                        ))}
                     </div>
             }
          </div>
          <ClosePageButton/>
          <Outlet/>
       </div>
   );
};

export default Profile;

