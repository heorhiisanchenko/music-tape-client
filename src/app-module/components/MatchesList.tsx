import React, {useEffect, useMemo, useState} from 'react';
import {LikeStats} from "../../shared-module/models/likes";
import likesApi from "../../shared-module/api/likes-api";
import Loader from "../../shared-module/components/Loader";
import './styles/MatchesList.css'
import {NavLink} from "react-router-dom";

const MatchesList = () => {

   const [likeStats, setLikeStats] = useState<LikeStats>();

   const map: { [key in keyof LikeStats]: { title: string, path: string } } = {
      matches: {title: "Matches", path: "matches"},
      sentLikes: {title: "Sent likes", path: "sent-likes"},
      receivedLikes: {title: "Received likes", path: "received-likes"},
   }

   useEffect(() => {
      likesApi.getStats().then(stats => {
         setLikeStats(stats)
      })
   }, [])

   const data = useMemo(() => {
      return likeStats && Object.entries(likeStats).filter(([key, value]) => value) as
          [keyof LikeStats, number][]
   }, [likeStats])

   return (
       <ul className="matches_list p-0 m-0 d-flex flex-column">
          {!data ? <Loader/> :
              data.map(([key, value]) => (
                  key === "matches" ? null :
                      <NavLink to={'/app/' + map[key]?.path} className="matches_item" key={key}>
                         <span className="matches_item_text">{map[key]?.title}: {value}</span>
                      </NavLink>
              ))}
       </ul>
   );
};

export default MatchesList;
