import React, {useState} from 'react';
import {useAuth} from "../../auth-module/providers/AuthProvider";
import './styles/MatchSidebar.css'
import ChatList from "../../chat-module/components/ChatList";
import {NavLink, useNavigate} from "react-router-dom";
import MatchesList from "./MatchesList";

enum ViewOption {
   MATCHES = "Matches",
   MESSAGES = "Messages",
}

const options = [ViewOption.MATCHES, ViewOption.MESSAGES,]

const MatchSidebar = () => {
   const {user} = useAuth();
   const [viewOption, setViewOption] = useState<ViewOption>(options[0]);
   const navigate = useNavigate()

   if (!user) return null;

   return (
       <div className="h-100 match_sidebar d-flex flex-column" style={{backgroundColor: 'aliceblue'}}>
          <div className="bg-warning text-center py-2 user_label"
               onClick={() => navigate('profiles/' + user?.id)}>User {user.id}</div>
          <div className="view_option_menu">
             {options.map(option => (
                 <button
                     key={option}
                     onClick={() => setViewOption(option)}
                     className={"view_option blank_button"}>
                    {option}
                 </button>
             ))}
             <span className={"selected_bottom_border " + viewOption.toLowerCase()}/>
          </div>
          <div className="matches_slider flex-grow-1">
             <div className={"matches_container h-100 " + viewOption.toLowerCase()}>
                <MatchesList/>
                <ChatList/>
             </div>
          </div>
       </div>
   );
};

export default MatchSidebar;
