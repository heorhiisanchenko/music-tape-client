import {IMedia} from "../models/media";

export const convertFileToMedia = (file: File): IMedia => {
   return {
      filename: file.name,
      type: file.type,
      src: URL.createObjectURL(file)
   }
}

