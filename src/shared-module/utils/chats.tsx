import {IChat} from "../models/chat";

export const getOtherUser = (chat: IChat, selfId: number) => chat.user1 === selfId ? chat.user2 : chat.user1;

export default {}
