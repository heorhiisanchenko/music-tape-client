import {instruments} from "../constants/instruments";

export const toggleBit = (num: number, bit: number) => {
   return num ^ (1 << bit);
}


export const getValuesOfMask = (mask: number, totalLength?: number) => {
   const values = mask
       .toString(2)
       .split('')
       .map(ch => parseInt(ch, 10))
       .reverse();
   const padding = totalLength ? totalLength - values.length : 0;
   return [...values, ...new Array(padding).fill(0)]
}


export const logMask = (mask: number) => {
   const values = getValuesOfMask(mask, instruments.length)
   console.log('')
   console.log(mask)
   instruments.forEach((instrument, i) => {
      console.log(`${instrument}: ${values[i]}`);
   })
}
