import {MouseEvent} from "react";

export const stopEvent = (fn: Function) => {
   return (e: MouseEvent<HTMLElement>) => {
      e.stopPropagation()
      e.preventDefault()
      fn()
   }
}
