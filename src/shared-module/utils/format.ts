import dayjs from "dayjs";


export const formatDateTime = (date: string, format: string) => {
   return dayjs(date).format(format);
}

export const formatUppercase = (string: string) => {
   return string[0].toUpperCase() + string.slice(1);
}
