export const saveToLocalStorage = (key: string, value: any) => {
    localStorage.setItem(key, value + '');
}

export const getFromLocalStorage = (key: string) => {
    return localStorage.getItem(key);
}

export const removeFromLocalStorage = (key: string) => {
    return localStorage.removeItem(key);
}
