import {combineReducers} from "@reduxjs/toolkit";
import chatReducer from "../../chat-module/reducers/chat-reducer";
import messageReducer from "../../chat-module/reducers/message-reducer";

export const rootReducer = combineReducers({
    messages: messageReducer,
    chats: chatReducer,
})
