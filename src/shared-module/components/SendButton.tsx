import React, {FC, MouseEvent} from 'react';

interface IProps {
   handleSubmit: (e: MouseEvent<HTMLButtonElement>) => any;
}

const SendButton: FC<IProps> = ({handleSubmit}) => {
   return (
       <div className="input_icon_container">
          <button
              type="submit"
              className="main_chat_send_button input_icon_container"
              onClick={handleSubmit}
          >
             <i className="fa-solid fa-paper-plane main_chat_send_icon"/>
          </button>
       </div>
   );
};

export default SendButton;
