import React, {useState, MouseEvent} from 'react';
import {Navbar, Nav, Button} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import LoginModal from "../../auth-module/components/LoginModal";
import SignupModal from "../../auth-module/components/SignupModal";
import {useAuth} from "../../auth-module/providers/AuthProvider";
import AuthButton from "../../auth-module/components/AuthButton";

interface IRoute {
   path: string,
   title: string
   requiresAuth?: boolean
}

const routes: IRoute[] = [
   {path: '/app', title: 'App', requiresAuth: true},
]

const NavBar = () => {
   const [showLogin, setShowLogin] = useState(false);
   const [showSignup, setShowSignup] = useState(false);

   const handleLogin = () => setShowLogin(true);
   const handleSignup = () => setShowSignup(true);

   const {user, logout} = useAuth();

   return (
       <Navbar className="" bg="primary" variant="dark" expand="sm">
          <Navbar.Text className="mx-4">
             <NavLink to="/">MusicTape</NavLink>
          </Navbar.Text>
          <Navbar.Toggle aria-controls="basic-navbar-nav"/>
          <Navbar.Collapse id="basic-navbar-nav" className="">
             <Nav className="mr-auto d-flex justify-content-start">
                {routes.map(route => (
                    (!route.requiresAuth || user) &&
                    <NavLink key={route.path} to={route.path} className="nav-link mx-1  px-auto">
                       {route.title}
                    </NavLink>
                ))}
             </Nav>
             <div className="flex-grow-1"/>
             <div className="mx-auto d-flex justify-content-center">
                {user ? <AuthButton onClick={logout}>Log out</AuthButton> :
                    <>
                       <AuthButton onClick={handleLogin}>Log in</AuthButton>
                       <AuthButton onClick={handleSignup}>Sign up</AuthButton>
                    </>
                }
             </div>
             <LoginModal show={showLogin} onHide={() => setShowLogin(false)}/>
             <SignupModal show={showSignup} onHide={() => setShowSignup(false)}/>
          </Navbar.Collapse>
       </Navbar>
   );
};

export default NavBar;
