import React from "react";


export const MicroIcon = () => {
    return (
        <div className="input_icon_container">
            <i className="fa-solid fa-microphone main_chat_icon"/>
        </div>
    )
}
