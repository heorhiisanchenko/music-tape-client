import {FC} from 'react';

interface IProps {
   handleLike: () => any;
   handleReject: () => any;
   className?: string
}

const LikeControls: FC<IProps> = ({handleLike, handleReject, className}) => {
   return (
       <div className={"tape_controls " + className || ""}>
          <div className="icon_button red" onClick={handleReject}>
             <i className="fa-solid fa-xmark tape_control_icon"/>
          </div>
          <div className="icon_button green" onClick={handleLike}>
             <i className="fa-solid fa-heart tape_control_icon"/>
          </div>
       </div>
   );
};

export default LikeControls;
