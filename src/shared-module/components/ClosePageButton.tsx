import React from 'react';
import {useNavigate} from "react-router-dom";
import './styles/CloseButton.css'

const ClosePageButton = () => {

   const navigate = useNavigate();

   const handleClick = () => {
      navigate('/app/recs')
   }

   return (
       <div className="close_button" onClick={handleClick}>
          <i className="fa-solid fa-xmark close_button_icon"/>
       </div>
   );
};

export default ClosePageButton;
