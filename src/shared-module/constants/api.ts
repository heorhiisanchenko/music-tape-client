// const PROXY_URL = 'http://localhost:9000/?url=';
const BASE_URL = window.location.origin + '/api';
// const CHAT_URL = 'http://localhost:4201';

export const urls = {
   BASE: BASE_URL,
   AUTH: BASE_URL + '/auth',
   USERS: BASE_URL + '/user',
   CHATS: BASE_URL + '/chat/chats',
   TAPES: BASE_URL + '/tape',
   // CHATS: CHAT_URL,
   LIKES: BASE_URL + '/likes',
}
