import {BaseAPI} from "./base-api";
import {urls} from "../constants/api";
import {ITape} from "../models/tapes";

class TapeApi extends BaseAPI {
   constructor() {
      super(urls.TAPES);
   }

   public async getTapesForUser(userId: number): Promise<ITape[]> {
      return this.get('/user', {params: {mask: userId}})
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async getRandomTape(mask: number): Promise<ITape> {
      return this.get('/get', {params: {mask}})
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async postTape(formData: FormData): Promise<any> {
      return this.post('/create', formData)
          .then(res => res.data)
          .catch(err => err.response.data)
   }
}

export default new TapeApi();
