import {urls} from "../constants/api"
import {IChat} from "../models/chat"

class UserApi {
   public async getUsersForChats(chats: IChat[]) {
      fetch(urls.USERS + '/get-by-array-id', {
         method: 'GET',
         body: JSON.stringify(chats)
      })
   }
}

export default new UserApi()
