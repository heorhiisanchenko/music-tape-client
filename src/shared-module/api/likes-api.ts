import {BaseAPI} from "./base-api";
import {urls} from "../constants/api";
import {ILike, IPartialLike, IUpdatedLike, LikeStats} from "../models/likes";

export class LikesAPI extends BaseAPI {
   constructor() {
      super(urls.LIKES + '');
   }

   public async getStats(): Promise<LikeStats> {
      return await this.get('/stats')
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async getSentLikes(): Promise<ILike[]> {
      return await this.get('/sent-likes')
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async getReceivedLikes(): Promise<ILike[]> {
      return await this.get('/received-likes')
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async getMatches(): Promise<ILike[]> {
      return await this.get('/matches')
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async postLike(like: IPartialLike): Promise<ILike> {
      return await this.post(`/`, like)
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async editLike(like: IUpdatedLike): Promise<any> {
      return await this.patch(`/${like.id}`, like)
          .then(res => res.data)
          .catch(err => err.response.data)
   }
}

export default new LikesAPI();
