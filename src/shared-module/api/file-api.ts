import {BaseAPI} from "./base-api";
import {urls} from "../constants/api";

class FileApi extends BaseAPI {

   constructor() {
      super(window.location.origin + '/uploads/videos');
   }

   public async getFile(filename: string): Promise<Blob> {
      const response = await this.get('/' + filename, {responseType: 'blob'});
      return response.data
   }

   public async postFile(formData: FormData) {
      return await this.post('', formData, {headers: {'Content-Type': 'multipart/form-data'}})
   }
}

export default new FileApi();
