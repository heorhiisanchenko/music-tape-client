import {BaseAPI} from "./base-api";
import {urls} from "../constants/api";
import {IChat} from "../models/chat";
import {IMessage, NoId} from "../models/message";

export class ChatAPI extends BaseAPI {
   constructor() {
      super(urls.CHATS + '');
   }

   public async getChats(): Promise<IChat[]> {
      return await this.get<IChat[]>('/')
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async createChat(chat: NoId<IChat>): Promise<IChat> {
      return await this.post('/', chat)
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async getMessages(chatId: number): Promise<IMessage[]> {
      return await this.get(`/${chatId}/messages`)
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async postMessage(message: NoId<IMessage>): Promise<IMessage> {
      return await this.post(`/${message.chatId}/messages`, message)
          .then(res => res.data)
          .catch(err => err.response.data)
   }
}

export default new ChatAPI();
