export interface IMedia {
   filename: string;
   type: string,
   src?: string
}
