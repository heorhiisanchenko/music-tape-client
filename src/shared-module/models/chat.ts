import {IMessage} from "./message"

export interface IChat {
    id: number,
    user1: number,
    user2: number
}

export interface IChatWithMessages extends IChat {
    messages?: IMessage[]
}
