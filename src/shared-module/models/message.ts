export interface IMessage {
    id: number
    content: string
    timestamp: string
    status: MessageStatus
    edited: boolean
    authorId: number
    recipientId: number
    chatId: number
}

export type NoId<T> = Omit<T, "id">

export enum MessageStatus {
    SENDING,
    DELIVERED,
    SEEN
}
