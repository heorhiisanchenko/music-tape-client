export interface ITape {
   id: number,
   user_id: number,
   mask: number,
   description: string,
   path: string
}

export interface INewTape {
   mask: number,
   description: string,
   video: File
}
