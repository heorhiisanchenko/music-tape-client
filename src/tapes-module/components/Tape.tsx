import React, {FC, useEffect, useState} from 'react';
import {ITape} from "../../shared-module/models/tapes";
import './styles/Tape.css'
import {getMediaByType} from "./media";
import Loader from "../../shared-module/components/Loader";
import {useLoadFile} from "../hooks/useLoadFile";
import likesApi from "../../shared-module/api/likes-api";
import {useAuth} from "../../auth-module/providers/AuthProvider";
import {LikeStatus} from "../../shared-module/models/likes";
import LikeControls from "../../shared-module/components/LikeControls";

enum ExitState {
   FALSE = "",
   RIGHT = "right",
   LEFT = "left"
}

interface IProps {
   tape: ITape
   top: boolean
   popTape: () => void;
}

const Tape: FC<IProps> = ({tape, top, popTape}) => {

   const {file, fetchFile} = useLoadFile();
   const {user} = useAuth();
   const [exit, setExit] = useState<ExitState>(ExitState.FALSE);

   useEffect(() => {
      fetchFile(tape.path)
   }, [])

   const handleLike = async (tape: ITape) => {
      if (!user) return;
      const newLike = {
         userId: user.id,
         likedUserId: tape.user_id,
         status: LikeStatus.SUBMITTED,
         created_at: new Date()
      }
      console.log(newLike)
      const result = await likesApi.postLike(newLike);
      console.log(result)
      setExit(ExitState.RIGHT)
      setTimeout(() => {
         popTape()
      }, 300)
   }

   const handleReject = async (tape: ITape) => {
      setExit(ExitState.LEFT)
      setTimeout(() => {
         popTape()
      }, 300)
   }

   return (
       <div className={"tape_position_container " + (exit)} style={top ? {zIndex: "2"} : {}}>
          <div className="tape_container">
             <div className="tape_video_container">
                {file ? <video className="tape_video" autoPlay>
                   <source src={file.src} type={file.type}/>
                   Video could not be played :(((
                </video> : <Loader/>}
             </div>
             <div className="tape_shadow_container">
                <div className="tape_main">
                   <div className="tape_info">
                      <h4>User {tape.user_id}</h4>
                      <div className="tape_description">{tape.description}</div>
                   </div>
                   <LikeControls
                       handleReject={() => handleReject(tape)}
                       handleLike={() => handleLike(tape)}
                   />
                </div>
             </div>
          </div>
       </div>
   );
};

export default Tape;

