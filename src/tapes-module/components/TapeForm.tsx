import React, {ChangeEvent, useRef} from 'react';
import {Button, Form} from 'react-bootstrap'
import fileApi from "../../shared-module/api/file-api";
import {convertFileToMedia} from "../../shared-module/utils/media";
import {saveToLocalStorage} from "../../shared-module/utils/local-storage";

const TapeForm = () => {

   const formDataRef = useRef<FormData | null>(null);

   const saveToApi = (file: File) => {
      const media = convertFileToMedia(file);
      saveToLocalStorage('media', media.filename)

      const formData = new FormData()
      formData.append('file', file)
      formDataRef.current = formData;
   }

   const handleChange = async (e: ChangeEvent<HTMLInputElement>) => {
      e.preventDefault()
      if (!e.target?.files) return;
      const file = e.target.files[0];
      saveToApi(file)
      // saveToStore(e.target.files[0])
   }

   const handleSubmit = (e: any) => {
      e.preventDefault();
      const formData = formDataRef.current;
      if (formData) fileApi.postFile(formData).then(res => {
      });
   }

   return (
       <Form>
          <label className="chat_file_input_label input_icon_container">
             <input
                 type="file" style={{display: 'none'}}
                 className="chat_file_input"
                 id="chat_file_input"
                 onChange={handleChange}/>
             <i className="fa-solid fa-paperclip add_media main_chat_icon"/>
          </label>
          <Button className="bg-primary" onClick={handleSubmit}>Send</Button>
       </Form>
   )
};

export default TapeForm;
