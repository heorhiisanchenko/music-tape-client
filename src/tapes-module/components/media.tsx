import React from "react";
import {IMedia} from "../../shared-module/models/media";

export const getMediaByType = (media: IMedia, className?: string) => {
   const type = media.type.split('/')[0]
   switch (type) {
      case 'image':
         return <img className={className} src={media.src} alt={media.filename}/>
      case 'video':
         return <video className={className} controls>
            <source src={media.src} type={media.type}/>
            Video could not be played :(((
         </video>
   }
}
