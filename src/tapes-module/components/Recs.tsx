import React, {useEffect, useMemo, useState} from 'react';
import {ITape} from "../../shared-module/models/tapes";
import fakeTapeService from "../services/fake-tape-service";
import Loader from "../../shared-module/components/Loader";
import Tape from "./Tape";
import "./styles/Recs.css"
import InstrumentForm from "./InstrumentForm";
import tapeApi from "../../shared-module/api/tape-api";

const Recs = () => {

   const [tapes, setTapes] = useState<ITape[]>();
   const [index, setIndex] = useState<number>(0);
   const [mask, setMask] = useState<number>(0);

   useEffect(() => {
      tapeApi.getRandomTape(mask)
          .then(tape => {
             if ("path" in tape)
                setTapes([...(tapes || []), tape])
          })
   }, [mask, index]);

   const popTape = () => {
      setIndex(index => index + 1)
   }

   const slicedTapes = useMemo(() => {
      return tapes && tapes.slice(index, index + 2);
   }, [index, tapes]);
   
   return (
       <div className="flex-grow-1 recs_container">
          <InstrumentForm mask={mask} setMask={setMask}/>
          <div className="tapes_list">
             {!slicedTapes ? <Loader/> : slicedTapes.map(tape =>
                 <Tape
                     popTape={popTape}
                     key={tape.id}
                     top={tape.id === slicedTapes[0]?.id}
                     tape={tape}
                 />
             )}
          </div>
       </div>
   );
};

export default Recs;

