import React, {ChangeEvent, FC, useEffect, useState} from 'react';
import fileApi from "../../shared-module/api/file-api";
import {Button, CloseButton, Form, Modal} from "react-bootstrap";
import {useNavigate} from "react-router-dom";
import './styles/CreateTapeForm.css'
import VideoPreview from "./styles/VideoPreview";
import {instruments} from "../../shared-module/constants/instruments";
import {toggleBit} from "../../shared-module/utils/mask";
import tapeApi from "../../shared-module/api/tape-api";

interface IProps {
   show?: boolean,
   onHide?: () => void
}

const CreateTapeForm: FC<IProps> = (props) => {
   const [file, setFile] = useState<File | null>(null);
   const [mask, setMask] = useState<number>(0);
   const [description, setDescription] = useState<string>('');
   const navigate = useNavigate()

   const handleChange = async (e: ChangeEvent<HTMLInputElement>) => {
      e.preventDefault()
      if (!e.target?.files) return;
      setFile(e.target.files[0]);
   }

   const handleSubmit = async (e: any) => {
      e.preventDefault();
      if (!file) return;
      const formData = new FormData()
      formData.append('video', file)
      formData.set('description', description)
      formData.set('mask', mask + '')
      await tapeApi.postTape(formData)
          .then(r => {
             console.log(r)
          })
      const formData2 = new FormData()
      formData2.append('file', file)
      navigate(-1)
   }

   return (
       <Modal show={true} onHide={() => navigate(-1)}
              size="lg" aria-labelledby="contained-modal-title-vcenter" centered
       >
          <CloseButton className="close" onClick={() => navigate(-1)}/>
          <Modal.Header>
             <Modal.Title id="contained-modal-title-vcenter">
                Create tape
             </Modal.Title>
          </Modal.Header>
          <Modal.Body>
             <Form noValidate>
                <Form.Group className="mb-3">
                   {file ? <VideoPreview file={file} setFile={setFile}/> :
                       <label className="tape_form_file_label">
                          <input
                              type="file" style={{display: 'none'}}
                              onChange={handleChange}/>
                          <i className="fa-solid fa-paperclip add_media main_chat_icon"/>
                       </label>
                   }
                </Form.Group>
                <Form.Group className="mb-3" controlId="">
                   <div>Instruments</div>
                   {instruments.map((instrument, index) => (
                       <Form.Check
                           inline
                           type="checkbox"
                           label={instrument}
                           onChange={() => setMask(toggleBit(mask, index))}
                       />
                   ))}
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                   <Form.Label>Description</Form.Label>
                   <Form.Control
                       type="textarea"
                       placeholder="Enter description"
                       value={description}
                       onChange={e => setDescription(e.target.value)}
                   />
                </Form.Group>
                <Button className="bg-primary" onClick={handleSubmit}>Send</Button>
             </Form>
          </Modal.Body>
       </Modal>
   )
};

export default CreateTapeForm;
