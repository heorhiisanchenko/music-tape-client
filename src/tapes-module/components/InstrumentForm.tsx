import React from 'react';
import {instruments} from "../../shared-module/constants/instruments";
import {Form} from "react-bootstrap";
import {toggleBit} from "../../shared-module/utils/mask";

const InstrumentForm = ({mask, setMask}: { mask: number, setMask: Function }) => {

   return (
       <Form>
          <Form.Group className="mb-3" controlId="">
             <div>Instruments</div>
             {instruments.map((instrument, index) => (
                 <Form.Check
                     key={index}
                     inline
                     type="checkbox"
                     label={instrument}
                     onChange={() => setMask(toggleBit(mask, index))}
                 />
             ))}
          </Form.Group>
       </Form>

   );
};

export default InstrumentForm;
