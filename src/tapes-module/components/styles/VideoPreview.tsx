import {Dispatch, FC, SetStateAction} from 'react';
import {getMediaByType} from "../media";
import {convertFileToMedia} from "../../../shared-module/utils/media";
import {CloseButton} from "react-bootstrap";

interface IProps {
   file: File
   setFile: Dispatch<SetStateAction<File | null>>
}

const VideoPreview: FC<IProps> = ({file, setFile}) => {
   return (
       <div className="tape_video_preview">
          <div className="tape_form_video_wrapper">
             {getMediaByType(convertFileToMedia(file), "tape_form_video")}
             <CloseButton className="tape_form_video_close" onClick={() => setFile(null)}/>
          </div>
       </div>
   );
};

export default VideoPreview;
