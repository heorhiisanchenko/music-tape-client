import {ITape} from "../../shared-module/models/tapes";
import {getFromLocalStorage} from "../../shared-module/utils/local-storage";

class FakeTapeService {

   private nextTape(id: number): ITape {
      return {
         id: id,
         user_id: id,
         description: "Lorem Ipsum " + id,
         mask: 1,
         path: getFromLocalStorage('media') || "ab.png"
      }
   }

   public async getTapes(): Promise<ITape[]> {
      return new Array(10).fill(0).map((_, i) => this.nextTape(i + 1))
   }
}

export default new FakeTapeService();
