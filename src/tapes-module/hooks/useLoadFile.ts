import {IMedia} from "../../shared-module/models/media";
import {useState} from "react";
import fileApi from "../../shared-module/api/file-api";

export const useLoadFile = () => {

   const [file, setFile] = useState<IMedia | null>(null);

   const fetchFile = async (filename: string) => {
      const respFile = await fileApi.getFile(filename)
      setFile({
         type: respFile.type,
         filename: filename,
         src: URL.createObjectURL(respFile),
      })
   }

   return {file, fetchFile};
}
