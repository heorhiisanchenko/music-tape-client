import {BaseAPI} from "../../shared-module/api/base-api";
import {urls} from "../../shared-module/constants/api";
import {LoginResponse, LoginValues, RegisterValues} from "../models/auth-models";
import axios from "axios";

export class AuthAPI extends BaseAPI {
   constructor() {
      super(urls.AUTH);
   }

   public async postLogin(values: LoginValues): Promise<LoginResponse> {
      return await this.post<LoginResponse>('/authenticate', values)
          .then(res => res.data)
          .catch(err => err.response.data)
   }

   public async postRegister(values: RegisterValues): Promise<any> {
      return await axios.post<LoginResponse>(urls.USERS + '/register', values)
          .then(res => res.data)
          .catch(err => err.response.data)
   }
}

export default new AuthAPI();
