import {createContext, useState, useContext, PropsWithChildren, FC, useEffect} from "react";
import authService from "../services/AuthService";
import {LoginValues, RegisterValues} from "../models/auth-models";
import {IUser} from "../../shared-module/models/user";
import {getFromLocalStorage, removeFromLocalStorage} from "../../shared-module/utils/local-storage";
import jwtDecode from "jwt-decode";

interface AuthContextType {
   user: IUser | null;
   login: (values: LoginValues) => Promise<IUser | { e: any }>
   logout: () => any;
   signup: (values: RegisterValues) => Promise<any>
}

const AuthContext = createContext<AuthContextType>(null!);

export const AuthProvider: FC<PropsWithChildren<any>> = ({children}) => {
   const [user, setUser] = useState<IUser | null>(null);

   const login = async (values: LoginValues) => {
      const user = await authService.login(values);
      if ("id" in user) {
         setUser(user);
      }
      return user;
   };

   const logout = async () => {
      //const result = await authService.logout();
      removeFromLocalStorage('token')
      setUser(null);
   };

   const signup = async (values: RegisterValues) => {
      const result = await authService.register(values);
      console.log(result);
      return result;
   };

   useEffect(() => {
      const token = getFromLocalStorage('token');
      if (!token) return;
      const user: IUser = jwtDecode(token)
      setUser(user)
   }, [])

   const value = {user, login, logout, signup};

   return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export const useAuth = () => {
   return useContext(AuthContext);
}
