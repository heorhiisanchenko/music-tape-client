import {LoginResponse, LoginValues, RegisterValues} from "../models/auth-models";
import authAPI, {AuthAPI} from "../api/AuthAPI";
import jwtDecode from "jwt-decode";
import {saveToLocalStorage} from "../../shared-module/utils/local-storage";
import {IUser} from "../../shared-module/models/user";

export class AuthService {

   constructor(private authAPI: AuthAPI) {
   }

   public async login(values: LoginValues): Promise<IUser | { e: any }> {
      try {
         const res = await this.authAPI.postLogin(values);
         console.log(res)
         const {token} = res;
         if (!token) return {e: res};
         saveToLocalStorage('token', token)
         const user: IUser = jwtDecode(token);
         return user;
      } catch (e: any) {
         return {e};
      }
   }

   public async register(values: RegisterValues) {
      return await this.authAPI.postRegister(values);
   }

   public async logout() {

   }
}


export default new AuthService(authAPI)
