export interface LoginValues {
    email: string;
    password: string;
}

export interface RegisterValues {
    nickname: string;
    email: string;
    password: string;
}

export interface LoginResponse {
    token: string
}
