import {Resolver} from "react-hook-form";
import {LoginValues} from "../models/auth-models";


export const resolver: Resolver<LoginValues> = async (values) => {
    return {
        values: values.email && values.password ? values : {},
        errors: {
            email: values.email ? {type: 'required', message: 'Email is required.'} : undefined,
            password: values.password ? {type: 'required', message: 'Password is required.'} : undefined,
        }
    };
};
