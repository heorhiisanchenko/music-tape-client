import {Modal, CloseButton, Form, Button} from "react-bootstrap";
import {useAuth} from "../providers/AuthProvider";
import {FC} from "react";
import {useForm} from "react-hook-form";
import {LoginValues, RegisterValues} from "../models/auth-models";


interface IProps {
   show: boolean,
   onHide: () => void
}

const SignupModal: FC<IProps> = (props) => {

   const {signup} = useAuth();
   const {register, handleSubmit} = useForm<RegisterValues>();
   const {onHide} = props;

   const onSubmit = async (data: RegisterValues) => {
      const res = await signup(data);
      onHide();
   }

   return (
       <Modal
           {...props}
           size="lg"
           aria-labelledby="contained-modal-title-vcenter"
           centered
       >
          <CloseButton className="close" onClick={props.onHide}/>
          <Modal.Header>
             <Modal.Title id="contained-modal-title-vcenter">
                Sign up
             </Modal.Title>
          </Modal.Header>
          <Modal.Body>
             <Form onSubmit={handleSubmit(onSubmit)} noValidate>
                <Form.Group className="mb-3" controlId="formBasicName">
                   <Form.Label>Username</Form.Label>
                   <Form.Control type="text" {...register("nickname")} placeholder="Enter a username"/>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control type="email" {...register("email")} placeholder="Enter email"/>
                   <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                   </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                   <Form.Label>Password</Form.Label>
                   <Form.Control type="password" {...register("password")} placeholder="Password"/>
                </Form.Group>

                <Button variant="primary" type="submit">
                   Submit
                </Button>
             </Form>
          </Modal.Body>
       </Modal>
   );
};

export default SignupModal;
