import {FC, PropsWithChildren} from 'react';
import {Button} from "react-bootstrap";

interface IProps {
    onClick: () => any
}

const AuthButton: FC<PropsWithChildren<IProps>> = ({onClick, children}) => {
    return (
        <Button
            variant="outline-light"
            onClick={onClick}
            className="mx-3"
        >
            {children}
        </Button>
    );
};

export default AuthButton;
