import {useAuth} from "../providers/AuthProvider";
import {Navigate} from 'react-router-dom'
import {getFromLocalStorage} from "../../shared-module/utils/local-storage";

export const RequireAuth = ({children}: { children: JSX.Element }) => {
   const {user} = useAuth();
   if (!getFromLocalStorage('token')) return <Navigate to="/"/>;
   return children;
}
