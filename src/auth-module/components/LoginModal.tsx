import React, {FC} from "react";
import {Modal, CloseButton, Form, Button} from "react-bootstrap";
import './styles/LoginModal.css'
import {useAuth} from "../providers/AuthProvider";
import {useForm} from "react-hook-form";
import {LoginValues} from "../models/auth-models";

interface IProps {
   show: boolean,
   onHide: () => void
}

const LoginModal: FC<IProps> = (props) => {

   const {login} = useAuth();
   const {register, handleSubmit} = useForm<LoginValues>();
   const {onHide} = props;

   const onSubmit = async (data: LoginValues) => {
      const res = await login(data);
      onHide();
   }

   return (
       <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
          <CloseButton className="close" onClick={onHide}/>
          <Modal.Header>
             <Modal.Title id="contained-modal-title-vcenter">
                Log in
             </Modal.Title>
          </Modal.Header>
          <Modal.Body>
             <Form onSubmit={handleSubmit(onSubmit)} noValidate>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                   <Form.Label>Email address</Form.Label>
                   <Form.Control type="email" {...register("email")} placeholder="Enter email"/>
                   <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                   </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                   <Form.Label>Password</Form.Label>
                   <Form.Control type="password" {...register("password")} placeholder="Password"/>
                </Form.Group>

                <Button variant="primary" type="submit">
                   Submit
                </Button>
             </Form>
          </Modal.Body>
       </Modal>
   );
};

export default LoginModal;
